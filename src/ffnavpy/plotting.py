import matplotlib.pyplot as plt
import numpy as np
import os


def plot_trajectory(trajectory):
    fig = plt.figure(figsize=[8, 8])
    fig.suptitle("A tale of 4 subplots")

    # First subplot
    ax = fig.add_subplot(2, 2, 1)

    ax.plot(trajectory[:, 0], trajectory[:, 1])
    ax.grid(True)
    ax.set_title("XY-plane")

    # First subplot
    ax = fig.add_subplot(2, 2, 2)

    ax.plot(trajectory[:, 0], trajectory[:, 2])
    ax.grid(True)
    ax.set_title("XZ-plane")

    # First subplot
    ax = fig.add_subplot(2, 2, 3)

    ax.plot(trajectory[:, 1], trajectory[:, 2])
    ax.grid(True)
    ax.set_title("YZ-plane")

    # Second subplot
    ax = fig.add_subplot(2, 2, 4, projection="3d")

    ax.plot(trajectory[:, 0], trajectory[:, 1], trajectory[:, 3])
    ax.grid(True)
    ax.set_title("3D")

    plt.show()

    return 0


def plot_poles(poles):
    """
    Plot the poles in the complex plane

    Parameters
    ----------
    poles : float, 1-D array (n) or list (n) (complex numbers)
        Poles to be plotted
    """

    fig, ax = plt.subplots()
    ax.scatter(np.real(poles), np.imag(poles))
    ax.axis("equal")
    ax.axhline(0, color="k")
    ax.axvline(0, color="k")

    return fig, ax


def plot_blocks(
    x,
    y,
    yhat=None,
    P=None,
    title="title",
    xlabel="xlabel",
    ylabels=[],
    xdstyle=None,
    fontdict=None,
    ylim=None,
):
    if xdstyle is None:
        xdstyle = {
            "color": "k",
            "linestyle": "--",
            "linewidth": 0.5,
            "marker": "+",
            "markersize": 4,
        }

    if fontdict is None:
        fontdict = {}

    ebarstyle = {"elinewidth": 0.5, "capsize": 2}

    n = len(y)
    axes = []

    fig = plt.figure(figsize=[8, 2 * n])

    fig.suptitle(title, fontdict=fontdict)

    for i in range(n):
        ax = fig.add_subplot(n, 1, i + 1)

        ax.plot(x, y[i], **xdstyle)
        if yhat is not None:
            if P is not None:
                ax.errorbar(x, yhat[i], P[i, i], fmt="b-", **ebarstyle)
            else:
                ax.plot(x, yhat[i], "b-")
        ax.set_xlabel(xlabel, fontdict=fontdict)
        ax.set_ylabel(ylabels[i], fontdict=fontdict)
        ax.ticklabel_format(axis='y', style='scientific', scilimits=(0, 3), useMathText=True)

        if ylim is not None:
            ax.set_ylim(ylim)

        axes.append(ax)

    fig.tight_layout()

    return fig, axes


def plot_position(
    x, y, yhat=None, P=None, units=("s", "m"), xdstyle=None, fontdict=None, ylim=None
):
    title = r"Position"
    xlabel = r"Time [{}]".format(units[0])
    ylabels = [
        label.format(units[1]) for label in [r"$x$ [{}]", r"$y$ [{}]", r"$z$ [{}]"]
    ]
    fig, ax = plot_blocks(x, y, yhat, P, title, xlabel, ylabels, xdstyle, fontdict, ylim)
    return fig, ax


def plot_velocity(
    x, y, yhat=None, P=None, units=("s", "m/s"), xdstyle=None, fontdict=None, ylim=None
):
    title = r"Velocity"
    xlabel = r"Time [{}]".format(units[0])
    ylabels = [
        label.format(units[1]) for label in [r"$x$ [{}]", r"$y$ [{}]", r"$z$ [{}]"]
    ]
    fig, ax = plot_blocks(x, y, yhat, P, title, xlabel, ylabels, xdstyle, fontdict, ylim)
    return fig, ax


def plot_control(
    x, y, yhat=None, P=None, units=("s", "m/s"), xdstyle=None, fontdict=None
):
    title = r"Control"
    xlabel = r"Time [{}]".format(units[0])
    ylabels = [
        label.format(units[1]) for label in [r"$x$ [{}]", r"$y$ [{}]", r"$z$ [{}]"]
    ]
    fig, ax = plot_blocks(x, y, yhat, P, title, xlabel, ylabels, xdstyle, fontdict)
    return fig, ax


def plot_state(x, y, yhat=None, P=None, units=("s", "m", "m/s")):
    title = "State"
    xlabel = "Time [{}]".format(units[0])
    ylabels = [label.format(units[1]) for label in ["$x$ [{}]", "$y$ [{}]", "$z$ [{}]"]]
    ylabels = ylabels + [
        label.format(units[2]) for label in ["$vx$ [{}]", "$vy$ [{}]", "$vz$ [{}]"]
    ]

    fig, ax = plot_blocks(x, y, yhat, P, title, xlabel, ylabels)
    return fig, ax


def plot_results(
    sim, xdstyle=None, fontdict=None, r_units=("s", "m"), v_units=("s", "m/s")
):
    """Plots all the defined figures and returns them in a dict"""
    fig_dict = {}
    time = sim.report.time
    position = sim.report.relative_state[:3]
    velocity = sim.report.relative_state[3:6]

    position_diff = sim.report.x_estimated[:3] - sim.report.relative_state[:3]
    velocity_diff = sim.report.x_estimated[3:6] - sim.report.relative_state[3:6]

    control = sim.report.u_cmd

    fig_dict["position"] = plot_position(
        time, position, xdstyle=xdstyle, fontdict=fontdict, units=r_units
    )
    fig_dict["velocity"] = plot_velocity(
        time, velocity, xdstyle=xdstyle, fontdict=fontdict, units=v_units
    )
    fig_dict["position_est_diff"] = plot_position(
        time, position_diff, xdstyle=xdstyle, fontdict=fontdict
    )
    fig_dict["velocity_est_diff"] = plot_velocity(
        time, velocity_diff, xdstyle=xdstyle, fontdict=fontdict, units=v_units
    )
    fig_dict["control"] = plot_control(
        time, control, xdstyle=xdstyle, fontdict=fontdict
    )

    return fig_dict


def save_plots(fig_dict, tag, analysis_label, fmt="pdf", dpi=300):
    """Save the plots stored in the fig dict"""
    for label, fig_tuple in fig_dict.items():
        file_path = analysis_label + "/"
        os.makedirs(file_path, exist_ok=True)
        fig_tuple[0].savefig(
            file_path + label + "_" + tag + "." + fmt, dpi=dpi, format=fmt
        )
