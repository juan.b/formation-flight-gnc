from ffnavpy.simulator import Simulator
from ffnavpy.simcase import ChiefDeputyCase
from ffnavpy.satellite import Satellite, CommandSequence
from ffnavpy.plotting import plot_results

import pickle
import os


def run_analysis(
    x0,
    dx0,
    x_target,
    tf,
    dT,
    configOBC,
    configSim,
    plot=False,
    save=False,
    cmd_seq=None,
    disable=False,
    perturbations=False
):
    """Run an analysis with the given configuration and returns the results"""

    # Initial state chief [km, km/s]
    x0_cs = x0 + dx0

    # Create satellites with their corresponding initial states
    LiteBird = Satellite(x0, 10, 1000)
    CalSat = Satellite(x0_cs, 0.15, 10)

    if cmd_seq is None:
        cmd_seq = CommandSequence()

    simcase = ChiefDeputyCase(
        LiteBird, CalSat, configOBC=configOBC, cmd_sequence=cmd_seq, optionsSim=configSim
    )
    sim = Simulator(simcase, dT, perturbations=perturbations)

    # Simulate
    sim.simulate(tf, disable)

    if plot:
        plot_results(sim)

    return sim


def save(data, file_name, dir_name):
    """
    Stores data in pickle object

    Parameters
    ----------
    data : -
        Data to be stored
    file_name : str
        File name
    dir_name : str
        Folder name
    """

    file_path = "Results/" + dir_name

    os.makedirs(file_path, exist_ok=True)

    file_handler = open(file_path + "/" + file_name, "wb")

    pickle.dump(data, file_handler)

    file_handler.close()

    return 0


def load(file_name, dir_name):
    """
    Recovers data from pickle object

    Parameters
    ----------
    file_name : str
        File name
    dir_name : str
        Folder name

    Returns
    -------
    data : -
        Data retrieved
    """

    file_path = "Results/" + dir_name
    file_handler = open(file_path + "/" + file_name, "rb")

    data = pickle.load(file_handler)

    file_handler.close()

    return data


def compute_ylimits(data):
    """
    Computes the adequate ylimits for the plot of data

    Parameters
    ----------
    data : ndarray
        Array with the data to be plotted

    Returns
    -------
    ylim : tuple (2)
        Symmetric limits for the plot
    """

    yp = data.max()
    yn = data.min()

    y = 1.1 * max(abs(yn), abs(yp))

    return (-y, y)
