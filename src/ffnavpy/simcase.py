from dataclasses import dataclass, asdict
from ffnavpy.satellite import OBC, CommandSequence, Formation
from ffnavpy.utils import cartesian_to_spherical, spherical_to_cartesian
from ffnavpy.report import Report

import numpy as np


class SimCase:
    """
    Abstract class for different simulation cases

    Attributes
    ----------
    satellites : list
        List of Satellite class instances
    wk : Workspace class
        Workspace of the simulation
    configSim : DefConfigSim class
        Configuration of the simulation

    Properties
    ----------
    sats : list
        List of satellites included in the simulation
    config_dict : dict
        Configuration of the simulation
    config_list : list
        Available configuration parameters of the simulation

    Methods
    -------
    propagate_step(propagator, step, dT)
        Propagates satellites for a time step
    task_step()
        Runs tasks to be performed in each step
    create_report()
        Creates empty Report class instance
    update_report(extra_data)
        Updates report, including more variables
    """

    def __init__(self, satellites, satellites_names=[], optionsSim={}):
        """
        Inits SimCase class

        Parameters
        ----------
        satellites : list/Satellite class
            Satellite class instances
        optionsSim : dict
            Configuration of the simulation
        """

        self.ff = Formation(satellites, satellites_names)

        try:
            self.configSim = self.DefConfigSim(**optionsSim)
        except TypeError:
            print("Error - Configuration not valid")

        self.wk = self.Workspace()

        for sat in self.sats.values():
            sat.add_OBC(OBC())

    @property
    def sats(self):
        """List of satellites included in the simulation"""
        return self.ff.sats

    @property
    def config_dict(self):
        """Configuration of the simulation"""
        return asdict(self.configSim)

    @property
    def config_list(self):
        """Available configuration parameters of the simulation"""
        return list(asdict(self.configSim).keys())

    def tasks_step(self):
        """Runs task to be performed in each step"""

        return 0

    def create_report(self):
        """
        Creates empty Report class instance

        Attributes
        ----------
        N : int
            Size of arrays
        """

        self.report = Report()

        return

    def update_report(self, extra_data={}):
        """
        Updates report, including extra variables

        Attributes
        ----------
        extra_data : dict
            Extra variables to include in the report
        """

        self.report.update({**vars(self.wk), **vars(extra_data)})

        return

    @dataclass
    class DefConfigSim:
        """
        DefConfigSim dataclass

        Default configuration of the simulator

        Attributes
        ----------
        sigma_distance : float
            Noise of sensors for range measurement
        sigma_angle : float
            Noise of sensors for angle measurement
        """

        sigma_distance: float = 0.1  # [mm]
        sigma_angle: float = 0.25  # [secs]
        sensors: bool = True

    @dataclass
    class Workspace:
        """
        Workspace dataclass

        Default configuration of the simulator

        Attributes
        ----------
        n : int
            Number of satellites
        sats_states : ndarray (6, m, n)
            Current states of the satellites (m steps, n satellites)
        dr : ndarray (3)
            Relative position
        d : float
            Distance between satellites
        du : ndarray (3)
            Relative position unitary vector

        Properties
        ----------
        states_step : ndarray (n, 6)
            Current satellites states
        """

        time: float = 0.0
        dr: np.ndarray = np.zeros(3)
        d: float = 0.0
        du: np.ndarray = np.zeros(3)
        thrust: np.ndarray = np.zeros(3)
        tb: float = 0.0


class ChiefDeputyCase(SimCase):
    """
    Chief-deputy formation case

    Attributes
    ----------
    satellites : list
        List of Satellite class instances
    wk : Workspace class
        Workspace of the simulation
    configSim : DefConfigSim class
        Configuration of the simulation
    chief : Satellite class
        Chief satellite
    deputy : Satellite class
        Deputy satellite
    OBC : OBC class
        On-Board Computer of the deputy

    Methods
    -------
    propagate_step(propagator, step, dT)
        Propagates satellites for a time step
    task_step()
        Runs tasks to be performed in each step
    create_report()
        Creates empty Report class instance
    update_report(extra_data)
        Updates report, including more variables
    compute_relative_distance()
        Pompute the relative position vector, its direction and its module
    sensors()
        Transforms the relative position vector into sensor measurements
    actuators(u_cmd)
        Applies maneuver to the deputy

    """

    def __init__(
        self, chief, deputy, configOBC={}, cmd_sequence=CommandSequence(), optionsSim={}
    ):
        """
        Inits ChiefDeputyCase class

        Parameters
        ----------
        chief : Satellite class
            Chief satellite
        deputy : Satellite class
            Deputy satellite
        configOBC : dict
            Configuration of the OBC
        cmd_sequence : CommandSequence class
            Sequence of commands to be executed during the simulation
        """

        super().__init__([chief, deputy], ["chief", "deputy"], optionsSim)
        self.chief = chief
        self.deputy = deputy
        self.wk = self.Workspace()
        self.OBC = OBC(configOBC, cmd_sequence)
        self.deputy.add_OBC(self.OBC)
        self.chief.add_OBC(OBC())

    def tasks_step(self):
        """Runs all the main tasks of the simulation"""
        self.retrieve_formation_states()
        self.compute_relative_distance()
        self.sensors()
        self.OBC.execute(self.wk.sensors_output)
        self.actuators()
        self.update_report(self.OBC.output)

        return 0

    def retrieve_formation_states(self):
        "Register current state and velocity"
        self.wk.chief_state = self.ff.sats["chief"].trajectory.states[:, -1]
        self.wk.deputy_state = self.ff.sats["deputy"].trajectory.states[:, -1]
        self.wk.time = self.ff.sats["deputy"].trajectory.time[-1]

    def compute_relative_distance(self):
        """Compute the relative position vector, its direction and its module"""

        self.wk.relative_state = self.wk.deputy_state - self.wk.chief_state
        self.wk.dr = self.wk.relative_state[:3]

        self.wk.spherical_coords = cartesian_to_spherical(self.wk.dr)
        self.wk.d = self.wk.spherical_coords[0]
        self.wk.du = self.wk.dr / self.wk.d

        return 0

    def sensors(self):
        """Transforms the relative position vector into sensor measurements"""

        if self.configSim.sensors:
            sigma_angle_rad = self.configSim.sigma_angle * np.pi / 180 / 3600

            # to meters
            sigma_distance = self.configSim.sigma_distance / 1e3

            noise_distance = np.random.normal(0, sigma_distance, size=1)
            noise_angles = np.random.normal(0, sigma_angle_rad, size=2)

            sensor_measurements = self.wk.spherical_coords + np.concatenate(
                (noise_distance, noise_angles)
            )

            self.wk.sensors_output = spherical_to_cartesian(sensor_measurements)

        else:
            self.wk.sensors_output = self.wk.relative_state

        return 0

    def actuators(self):
        """
        Computes the force generated by the actuators according to the control
        signal.

        Parameters
        ----------
        u_cmd : ndarray (3)
            Control signal from the control algorithm
        controller_type : str
            Type of control (continuous/discrete)
        """

        # Transforms control signal to dv

        # generate noise

        return 0

    @dataclass
    class Workspace:
        """
        Workspace dataclass

        Default configuration of the simulator

        Attributes
        ----------
        n : int
            Number of satellites
        sats_states : ndarray (6, m, n)
            Current states of the satellites (m steps, n satellites)
        dr : ndarray (3)
            Relative position
        d : float
            Distance between satellites
        du : ndarray (3)
            Relative position unitary vector

        Properties
        ----------
        states_step : ndarray (n, 6)
            Current satellites states
        """

        deputy_state: np.ndarray = np.zeros(6)
        chief_state: np.ndarray = np.zeros(6)
        relative_state: np.ndarray = np.zeros(6)
        time: float = 0.0
        dr: np.ndarray = np.zeros(3)
        d: float = 0.0
        du: np.ndarray = np.zeros(3)
        thrust: np.ndarray = np.zeros(3)
        tb: float = 0.0
        spherical_coords: np.ndarray = np.zeros(3)
        sensors_output: np.ndarray = np.zeros(3)
