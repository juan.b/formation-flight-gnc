import numpy as np
import scipy
from abc import ABC, abstractmethod


class NavigationAlgorithm(ABC):
    @abstractmethod
    def estimate_step(self):
        pass


class TrueObserver(NavigationAlgorithm):
    """Dummy navigation algorithm which provides true state"""

    @staticmethod
    def estimate_step(u, y, *args, **kwargs):
        """
        Returns null control signal

        Returns
        -------
        u_cmd : ndarray (3)
            Null control signal

        """
        return y, None


class SimpleObserver(NavigationAlgorithm):
    """
    SimpleObserver class

    Attributes
    ----------
    A : ndarray (n, n)
        System matrix
    B : ndarray (n, b)
        Input matrix
    C : ndarray (a, n)
        Output matrix
    poles : ndarray (6)
        Simpler observer poles
    xhat_km1: ndarray (n)
        Guess of the previous state
    Tsk : float
        Duration of OBC cycle [-]

    Methods
    -------
    estimate_step(u, y)
        Estimates the current state of the satellite
    compute_L()
        Computes observer gain matrix L
    """

    def __init__(self, A, B, C, poles, x0, Tsk):
        """
        Inits SimpleObserver class

        Parameters
        ----------
        A : ndarray (6, 6)
            System matrix
        B : ndarray (6, 3)
            Input matrix
        C : ndarray (3, 6)
            Output matrix
        poles : ndarray (6)
            Simpler observer poles
        x0: ndarray (6)
            Guess of the initial state
        L: ndarray (6, 3)
            Observer gain
        Tsk : float
            Duration of OBC cycle [-]
        """

        self.A = A
        self.B = B
        self.C = C
        self.poles = poles
        self.xhat_km1 = x0
        self.L = self.compute_L()
        self.Tsk = Tsk

    def estimate_step(self, u, y):
        """
        Estimates state of the system

        Attributes
        ----------
        u : ndarray (b, 1)
            Control vector
        y : ndarray (a, 1)
            Measurement vector

        Returns
        -------
        xhat : ndarray (n)
            Estimated state
        """

        yhat = self.C @ self.xhat_km1
        xhat_dot = self.A @ self.xhat_km1 + self.B @ u / self.Tsk - self.L @ (yhat - y)

        xhat = self.xhat_km1 + xhat_dot * self.Tsk

        self.xhat_km1 = xhat

        return xhat, None

    def compute_L(self):
        """
        Computes the observer gain matrix

        Returns
        -------
        L : ndarray (6, 3)
            observer gain matrix
        """

        L = scipy.signal.place_poles(
            self.A.T, self.C.T, self.poles, maxiter=30, rtol=-1
        ).gain_matrix.T

        return L


class KalmanFilter(NavigationAlgorithm):
    """
    Kalman filter class

    Attributes
    ----------
    A : ndarray (n, n)
        System matrix
    B : ndarray (n, b)
        Input matrix
    C : ndarray (a, n)
        Output matrix
    V : ndarray
        Process noise matrix
    W : ndarray
        Sensors noise matrix
    x0: ndarray (n)
        Guess of the initial state
    P0 : ndarray (n, n)
        Initial covariance matrix
    Tsk : float
        Duration of OBC cycle [-]
    step_count : int
        Number of estimation steps carried out
    xkkm1 : ndarray (n)
        Previous step (k-1) state
    Pkkm1 : ndarray (n, n)
        Previous step (k-1) covariance matrix

    Methods
    -------
    status()
        Prints estimator status
    predict_step(u)
        Performs filter prediction step
    correct_step(xkn, Pkn, y)
        Performs filter correction step
    estimate_step(u, y)
        Estimates the current state of the satellite
    reset(x0, P0)
        Resets the kalman filter counter and optionally x0 and P0
    """

    def __init__(self, A, B, C, V, W, x0, P0, Tsk):
        """
        Inits Command class

        Parameters
        ----------
        A : ndarray (n, n)
            System matrix
        B : ndarray (n, b)
            Input matrix
        C : ndarray (a, n)
            Output matrix
        V : ndarray
            Process noise matrix
        W : ndarray
            Sensors noise matrix
        x0: ndarray (n)
            Guess of the initial state
        P0 : ndarray (n, n)
            Initial covariance matrix
        Tsk : float
            Duration of OBC cycle [-]
        """

        self.A = A
        self.B = B
        self.C = C
        self.V = V
        self.W = W
        self.STM = np.eye(6) + A * Tsk
        self.x0 = x0
        self.P0 = P0
        self.Tsk = Tsk
        self.step_count = 0

        self.xkkm1 = None
        self.Pkkm1 = None

    def status(self):
        """Prints estimator status"""
        print("Status summary")
        print("Step" + str(self.step_count))
        print("Estimated state (k-1)")
        print(self.xkkm1)
        print(self.Pkkm1)

    def predict_step(self, u):
        """
        Performs filter prediction step

        Parameters
        ----------
        u : ndarray (b, 1)
            Control vector

        Returns
        -------
        xkn : ndarray (n)
            Predicted state
        Pkn : ndarray (n, n)
            Predicted covariance matrix
        """

        if self.step_count == 0:
            xkn = self.x0
            Pkn = self.P0
        else:
            xkn = self.STM @ self.xkkm1 + self.B @ u
            Pkn = self.STM @ self.Pkkm1 @ self.STM.T + self.W

        return xkn, Pkn

    def correct_step(self, xkn, Pkn, y):
        """
        Performs filter correction step

        Parameters
        ----------
        xkn : ndarray (n)
            Predicted state
        Pkn : ndarray (n, n)
            Predicted covariance matrix
        y : ndarray (a, 1)
            Measurement vector

        Returns
        -------
        xkp : ndarray (n)
            Corrected state
        Pkp : ndarray (n, n)
            Corrected covariance matrix
        """

        K = Pkn @ self.C.T @ np.linalg.inv(self.V + self.C @ Pkn @ self.C.T)
        xkp = xkn - K @ (self.C @ xkn - y)
        Pkp = Pkn - K @ self.C @ Pkn

        return xkp, Pkp

    def estimate_step(self, u, y):
        """
        Estimates state of the system

        Attributes
        ----------
        u : ndarray (b, 1)
            Control vector
        y : ndarray (a, 1)
            Measurement vector

        Returns
        -------
        xhat : ndarray (n)
            Estimated state
        P : ndarray (n, n)
            Estimated covariance matrix
        """

        xkn, Pkn = self.predict_step(u)
        xhat, P = self.correct_step(xkn, Pkn, y)
        self.xkkm1, self.Pkkm1 = xhat, P
        self.step_count += 1

        return xhat, P

    def reset(self, x0=None, P0=None):
        """
        Resets the kalman filter counter and optionally x0 and P0

        Parameters
        ----------
        x0: ndarray (n)
            Guess of the initial state
        P0 : ndarray (n, n)
            Initial covariance matrix
        """

        self.step_count == 0
        if x0 is not None:
            self.x0 = x0
        if P0 is not None:
            self.P0 = P0

        return 0
