from ffnavpy.dynamics import Dynamics

import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm


class Simulator:
    """
    Simulator class

    Top class to gather and connect all simulation components,
    launch simulations and plot results

    Attributes
    ----------
    dynamics : Dynamics class
        Configuration of the OBC
    dT : float
        Controller algorithm
    simcase : Simcase class
        Configuration of the OBC
    report : Report class
        Controller algorithm

    Properties
    ----------
    sats : list
        Satellites included in the simulator

    Methods
    -------
    simulate(tf)
        Creates trajectory class instance
    simulate_step(idx)
        Returns position of satellite for given time
    plot()
        Returns velocity of satellite for given time
    """

    def __init__(
        self,
        simcase,
        dT=0.1,
        model="CR3BP",
        report=True,
        library="mubody",
        perturbations=None,
    ):
        """
        Inits Simulator class

        Parameters
        ----------
        simcase : Simcase class
            Specific simulation case
        dT : float
            Time step for the integration
        model : str
            Name of the dynamics model to be used (CR3BP/NBP)
        report : bool
            If True, creates Report class instance
        library : str
            Name of the library to be used (sempy/mubody)
        """

        self.dynamics = Dynamics(model, library, perturbations)
        self.propagator = self.dynamics.propagator
        self.dT = dT
        self.simcase = simcase
        self.report_flag = report
        self._idx = 0
        self._allowed_dT = np.array([0.01, 0.1, 0.2, 0.5, 1.0, 10.0, 100.0])

        self.check_dT()

    @property
    def sats(self):
        """Sats included in the simulation"""
        return self.simcase.sats

    @property
    def time(self):
        """Global simulator clock"""
        return self._idx * self.dT

    @property
    def report(self):
        """Access to the report"""
        return self.simcase.report

    def _counter_step(self):
        """Advances clock one step"""
        self._idx += 1

    def _reset_counter(self):
        self._idx = 0

    def check_dT(self):
        if any(np.isclose(self._allowed_dT - self.dT, 0)):
            pass
        else:
            raise ValueError("Value of dT not allowed")

    def simulate(self, tf, disable=False):
        """
        Carry out the simulation, performing all the required task in each time step

        Parameters
        ----------
        tf : float
            Simulation time
        """

        self.N = int(np.ceil(tf / self.dT)) + 1
        self._reset_counter()

        if self.report_flag:
            self.simcase.create_report()

        for _ in tqdm(range(self.N), disable=disable):
            self.simulate_step()
            self._counter_step()

        # for sat in self.sats.values():
        #     sat.trajectory.generate_tck()

        return 0

    def simulate_step(self):
        """
        Performs all the task for a given step
        """

        code_tasks = self.simcase.tasks_step()

        code_prop = self.propagate_step()

        self.sim_codes = (code_prop, code_tasks)

        return 0

    def propagate_step(self):
        """
        Propagates the satellites a time step
        """

        for sat in self.simcase.sats.values():
            new_state = self.propagator(sat, self.dT)
            new_time = self.time + self.dT
            if sat.OBC.output.tb > 0:
                sat.OBC.output.tb -= self.dT
                if sat.OBC.output.tb < 0:
                    sat.OBC.output.tb = 0
            # Last propagation is discarded
            if self._idx < (self.N - 1):
                sat.trajectory.update(new_time, new_state)

        return 0

    def plot(self):
        """Plots satellite trajectories"""

        figs = []
        axes = []
        for sat in self.sats:
            fig, ax = plt.subplots()
            ax.plot(sat.trajectory.states[:, 0], sat.trajectory.states[:, 1])
            figs.append(fig)
            axes.append(ax)

        return figs, axes
