from abc import ABC, abstractmethod

import control as ct
import numpy as np
import scipy
import scipy.integrate as sci
from qpsolvers import Problem, solve_problem

from ffnavpy.utils import (
    build_A_ineq,
    build_A_stack,
    build_b_ineq,
    build_B_stack,
    build_Q_stack,
    build_R_stack,
)


class ControlAlgorithm(ABC):
    """Abstract class for a control algorithm"""

    @abstractmethod
    def run(self):
        """
        Runs control algorithm and returns the control signal

        Returns
        -------
        u_cmd : ndarray (3)
            Control signal
        """
        u_cmd = np.zeros(3)
        return u_cmd


class DummyControl(ControlAlgorithm):
    """Dummy algorithm which does not apply control"""

    @staticmethod
    def run(*args, **kwargs):
        """
        Returns null control signal

        Returns
        -------
        u_cmd : ndarray (3)
            Null control signal

        """
        return np.zeros(3)


class LinearRegulator(ControlAlgorithm):
    """
    LinearRegulator class

    u = - K @ x + k @ yref

    Attributes
    ----------
    A : ndarray (6, 6)
        System matrix
    B : ndarray (6, 3)
        Input matrix
    C : ndarray (3, 6)
        Output matrix
    poles : ndarray (6)
        Linear regulator poles
    k : ndarray (3, 6)
        k matrix
    K : ndarray (3, 6)
        Controller gain matrix

    Methods
    -------
    run(X, yref)
        Runs control algorithm and returns the control signal
    compute_k()
        Computes k matrix, which locates the equilibrium point at yref
    compute_K()
        Computes que controller gain matrix
    """

    def __init__(self, A, B, C, poles):
        """
        Inits LinearRegulator class

        Parameters
        ----------
        A : ndarray (n, n)
            System matrix
        B : ndarray (n, b)
            Input matrix
        C : ndarray (a, n)
            Output matrix
        """

        self.A = A
        self.B = B
        self.C = C
        self.poles = poles
        self.K = self.compute_K()
        self.k = self.compute_k()

    def run(self, x, xref):
        """
        Runs control algorithm and returns the control signal.

        Parameters
        ----------
        x : ndarray
            Estimated state
        xref : ndarray
            Target state

        Returns
        -------
        u_cmd : ndarray (3)
            Control signal
        """

        yref = self.C @ xref

        u = -self.K @ x + self.k @ yref

        return u

    def compute_k(self):
        """
        Computes k matrix, which locates the equilibrium point at yref

        Returns
        -------
        k : ndarray (3, 6)
            k matrix
        """

        k = -np.linalg.inv(self.C @ np.linalg.inv(self.A - self.B @ self.K) @ self.B)

        return k

    def compute_K(self):
        """
        Computes que controller gain matrix

        Returns
        -------
        K : ndarray (3, 6)
            Controller gain matrix
        """

        K = scipy.signal.place_poles(
            self.A, self.B, self.poles, maxiter=30, rtol=-1
        ).gain_matrix

        return K


class LQR(LinearRegulator):
    """
    LQR class

    Attributes
    ----------
    A : ndarray (6, 6)
        System matrix
    B : ndarray (6, 3)
        Input matrix
    C : ndarray (3, 6)
        Output matrix
    poles : ndarray (6)
        Linear regulator poles
    k : ndarray (3, 6)
        k matrix
    K : ndarray (3, 6)
        Controller gain matrix
    Q : ndarray (6, 6)
        Weighting matrix for states
    R : ndarray (3, 3)
        Weighting matrix for control

    Methods
    -------
    run(X, yref)
        Runs control algorithm and returns the control signal
    compute_k()
        Computes k matrix, which locates the equilibrium point at yref
    compute_K()
        Computes que controller gain matrix
    """

    def __init__(self, A, B, C, Q, R):
        """
        Inits LinearRegulator class

        Parameters
        ----------
        A : ndarray (n, n)
            System matrix
        B : ndarray (n, b)
            Input matrix
        C : ndarray (a, n)
            Output matrix
        Q : ndarray (6, 6)
            Weighting matrix for states
        R : ndarray (3, 3)
            Weighting matrix for control
        """

        self.Q = Q
        self.R = R
        LinearRegulator.__init__(self, A, B, C, None)

    def compute_K(self):
        """
        Computes que controller gain matrix

        Returns
        -------
        K : ndarray (3, 6)
            Controller gain matrix
        """

        K, _, _ = ct.lqr(self.A, self.B, self.Q, self.R, method="scipy")

        return K


class ETM(ControlAlgorithm):
    """
    ETM class

    Attributes
    ----------
    A : ndarray (n, n)
        System matrix
    period : float
        Time period between maneuvers [-]
    mu : float
        CR3BP parameter

    Methods
    -------
    ode(time, state_expanded)
        Returns ode of state+STM
    state_dot(state)
        Computes derivative of the state
    stm_dot(state, stm)
        Computes derivateive of STM matrix
    run(X, yref)
        Runs control algorithm and returns the control signal
    targeting_method(X0, yref, i_max)
        Runs targeting method
    """

    def __init__(self, A, period):
        """
        Inits ETM class

        Parameters
        ----------
        A : ndarray (n, n)
            System matrix
        period : float
            Time period between maneuvers [-]
        mu : float
            CR3BP parameter
        """

        self.A = A
        self.period = period

    def ode(self, _, state_expanded):
        """
        Return the derivates of both state and STM matrix as a single vector

        Parameters
        ----------
        time : float
            Time of interest
        state_expanded : ndarray (6+36)
            State and STM concatenated as a single vector

        Returns
        -------
        ode : ndarray (6+36)
            Derivates of state and STM matrix as a single vector
        """

        ode = np.concatenate(
            (
                self.state_dot(state_expanded[:6]),
                self.stm_dot(state_expanded[:6], state_expanded[6:]),
            )
        )

        return ode

    def state_dot(self, state):
        """
        Computes state derivative using A matrix

        Parameters
        ----------
        state : ndarray (6)
            State of satellite

        Returns
        -------
        state_dot : ndarray (6)
            Derivative of state
        """

        state_dot = self.A @ state

        return state_dot

    def stm_dot(self, state, stm):
        """
        Computes STM derivative using A matrix

        Parameters
        ----------
        state : ndarray (36)
            State transition matrix (STM)

        Returns
        -------
        state_dot : ndarray (36)
            Derivative of STM
        """

        stm_dot = self.A @ stm.reshape(6, 6)

        return stm_dot.reshape(36)

    def run(self, x0, yref):
        """
        Runs control algorithm and returns the control signal

        Parameters
        ----------
        x0 : ndarray (6)
            State at the start of the segment
        yref : ndarray (6)
            Target state at the end of the segment

        Returns
        -------
        u_cmd : ndarray (3)
            Control signal
        """

        dv = self.targeting_method(x0, yref)

        return dv

    def targeting_method(self, x0, yref):
        """
        Targeting method

        Parameters
        ----------
        x0 : ndarray (6)
            State at the start of the segment
        yref : ndarray (6)
            Target state after the period

        Returns
        -------
        dv : ndarray (3)
            Dv needed to arrive at the desired location
        """

        # run control algorithm
        t_span = (0, self.period)
        error_tol = 1e-14

        u = np.zeros(3)
        X = np.copy(x0)

        for _ in range(100):
            X[3:6] = X[3:6] + u
            y0 = np.concatenate((X, np.eye(6).reshape(36)))

            sol = sci.solve_ivp(
                fun=self.ode,
                t_span=t_span,
                y0=y0,
                t_eval=t_span,
                first_step=1e-11,
                method="DOP853",
                rtol=1e-12,
                atol=1e-12,
            )

            vector = sol.y

            yfin = vector[0:3, -1]

            phi = vector[6:, -1].reshape(6, 6)[0:3, 3:6]

            error_v = yref[:3] - yfin
            u = np.linalg.inv(phi) @ error_v

            error_m = np.linalg.norm(error_v)

            if error_m < error_tol:
                break

        dv = X[3:6] - x0[3:6]

        return dv


class MPC(ControlAlgorithm):
    """
    Model Predictive class

    Attributes
    ----------
    A : ndarray (6, 6)
        System matrix
    B : ndarray (6, 3)
        Input matrix
    C : ndarray (3, 6)
        Output matrix
    Q : ndarray (6, 6)
        Weighting matrix for states
    R : ndarray (3, 3)
        Weighting matrix for control
    N : int
        Prediction Horizon length
    u_threshold : int
        Max/min value for control
    Tsk : int
        Time duration of each control step
    CA : bool
        If True, collision avoidance constraint is implemented
    Rkoz : float
        Radius of the safety sphere

    Methods
    -------
    run(X, yref)
        Runs control algorithm and returns the control signal
    mpc_controller(X, yref)
        MPC controller algorithm
    """

    def __init__(
        self, A, B, C, Q, R, N, u_threshold, Tsk, collision_avoidance=False, Rkoz=0
    ):
        """
        Inits LinearRegulator class

        Parameters
        ----------
        A : ndarray (6, 6)
            System matrix (STM)
        B : ndarray (6, 3)
            Input matrix
        C : ndarray (3, 6)
            Output matrix
        Q : ndarray (6, 6)
            Weighting matrix for states
        R : ndarray (3, 3)
            Weighting matrix for control
        N : int
            Prediction Horizon length
        u_threshold : int
            Max/min value for control
        Tsk : int
            Time duration of each control step
        CA : bool
            If True, collision avoidance constraint is implemented
        Rkoz : float
            Radius of the safety sphere
        """

        self.A = A
        self.B = B
        self.C = C
        self.Q = Q
        self.R = R
        self.N = N
        self.u_threshold = u_threshold
        self.Tsk = Tsk
        self.CA = collision_avoidance
        self.Rkoz = Rkoz

        self.A_stack = build_A_stack(self.A, self.N)
        self.B_stack = build_B_stack(self.A, self.B, self.N)
        self.Q_stack = build_Q_stack(self.Q, self.N)
        self.R_stack = build_R_stack(self.R, self.N)

    def run(self, x0, yref):
        """
        Runs control algorithm and returns the control signal

        Parameters
        ----------
        x0 : ndarray (6,)
            State at the start of the segment [-, -]
        yref : ndarray (6,)
            Target state at the end of the segment [-, -]

        Returns
        -------
        u : ndarray (3,)
            Control signal [-]
        """

        u = self.mpc_controller(x0, yref)

        return u

    def mpc_controller(self, x0, yref):
        """
        Model predictive controller.

        Parameters
        ----------
        x0 : ndarray (6,)
            State at the start of the segment [-, -]
        yref : ndarray (6,)
            Target state at the end of the segment [-, -]

        Returns
        -------
        u : ndarray (3,)
            Control signal [-]
        """

        DeltaX = x0 - yref

        P = self.B_stack.T @ self.Q_stack @ self.B_stack + self.R_stack

        q = (DeltaX.T.dot(self.A_stack.T) @ self.Q_stack @ self.B_stack).T

        # Constraints of control
        umax = np.repeat(self.u_threshold, self.N, axis=0)
        umin = -umax

        if self.CA:
            # add collision avoidance constraint and rescale the problem
            G0 = build_A_ineq(x0, self.B_stack, self.C, self.N)
            h0 = build_b_ineq(x0, self.A_stack, self.C, self.Rkoz, self.N)

            # factor_a = 10 ** np.abs(np.log10(np.linalg.norm(h0[h0 != 0], ord=-np.inf)))
            # factor_b = 10 ** np.abs(np.log10(np.linalg.norm(G0[G0 != 0], ord=-np.inf)))
            # factor_c = factor_a / factor_b

            # G = G0 * factor_b
            # h = h0 * factor_a

            # P = P / factor_c**2
            # q = q / factor_c
            # umin = umin * factor_c
            # umax = umax * factor_c

            G = G0
            h = h0
            factor_c = 1

        else:
            G = None
            h = None
            factor_c = 1

        problem = Problem(P, q, G, h, lb=umin, ub=umax)
        solution = solve_problem(problem, solver="cvxopt")

        # apply only the first maneuver
        u = solution.x[:3] / factor_c

        return u
