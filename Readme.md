# GNC for a formation flight in L2

## Introduction

The proposed topic for this project is the design of the GNC system for the deputy in a chief-deputy formation in L2, focusing in the design of the control law and the assessment of its performance under a high fidelity simulation environment (ephemeris and SRP).

## Motivation

The use of formation flight (FF) in space missions is drawing increasing attention in recent years because of the advantages it offers and the potential for new missions. The distribution of functions among multiple spacecrafts which operate coordinately can increase the science return, the redundancy of the mission and its flexibility compared with a single large and complex satellite.

The main challenge associated to FF is the design of the GNC system that complies with the requirements of the mission, which are becoming more demanding, particularly in science missions.

One of the destinations for FF missions with more potential is the Libration Point Orbits (LPO), commonly used for deep-space observation missions which could benefit from FF in terms of calibration, use of interferometry techniques, use of occulters, etc.

The GNC system for a mission in LPO has different challenges compared than in a FF orbiting Earth: GPS unavailable, complex dynamics environment and low thrust requirements.

## Description

The mission under study is a chief-deputy formation in which the chief is a CMB telescope which needs to be periodically calibrated. The deputy is a CubeSat which will be deployed from the chief once it has reached its nominal orbit. The deputy has two modes of operation, stand-by and calibration, each one with different requirements of accuracy. In the former mode, the deputy stays out of the observation zone of the chief, keeping loosely the formation. In the calibration mode, the chief moves to a predefined position in the observation zone, where it emits a well-known reference signal to be received by the chief’s instrument. In this mode, the formation must be kept more tightly, with a margin error of cm or m (depending of the size of the formation).

In this work, the GNC system for this mission will be design and simulated under a high fidelity dynamical model, provided by the Sempy library. There is a preliminary version of the GNC system, developed in Simulink, which consists on an EKF with a LQR. This system does not differentiate between the two modes nor does includes the maneuvering. The first step will be moving the current system to Python and implement a discrete control for the stand-by mode and a maneuvering strategy to change between modes. Furthermore, the feasibility of using a H-inf controller for the calibration mode will be studied. Finally, the improved GNC system will be tested using Sempy’s full ephemeris model with the SRP perturbation included.

If the project progresses fast enough, it is also proposed to test the robustness of the controllers under thrusting errors and to incorporate a system of collision avoidance.


## Objectives

* Develop a GNC system for the mission
* Design maneuvering between operation modes
* Assess control performance under perturbations 

## Planification

### Main tasks

* Incorporate perturbations to Sempy
* Implement existing GNC system in Python (EKF+LQR)
* Impement ETM for the stand-by mode
* Design maneuvering between modes
* Study the feasibility of using a Hinf controller for the calibration mode 
* Assess the formation control performance under perturbations (high fidelity model)

### Secondary tasks

* Study the robustness of the proposed control under thrusting errors
* Incorporate collision avoidance 





