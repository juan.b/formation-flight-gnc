import numpy as np
import numpy.testing as npt
import unittest
from ffnavpy.dynamics import Dynamics
from mubody.mission import Mission
from ffnavpy.satellite import Satellite, OBC


class PropagatorTest(unittest.TestCase):
    def setUp(self):
        # Setup parameters and mubody mission
        self.model_ref = "CR3BP"
        self.library_ref = "mubody"
        self.tf = 3600  # [s]
        self.mubody_mission = Mission(SRP_flag=False, mission_time=self.tf)
        self.mubody_mission.IC(frame="P1-IdealSynodic", bar=False)
        self.x0 = self.mubody_mission.sat.orbit.IC()[0].flatten()

        self.rvs_ref = np.array(
            [
                1.495980230000000e11,
                1.495980230000000e11,
                1.495980230000000e11,
                2.978917548038634e04,
                2.978917548038634e04,
                2.978917548038634e04,
            ]
        )  # [m, m/s]

        self.x0_adim = self.x0 / self.rvs_ref
        self.tf_adim = self.tf / (self.rvs_ref[0] / self.rvs_ref[-1])

        self.CR3BP_dynamics = Dynamics(
            model=self.model_ref, library=self.library_ref, perturbations=None
        )

        self.sat = Satellite(self.x0)
        self.sat.add_OBC(OBC())

    def test_Dynamics(self):
        # Check dynamics for CR3BP are created correctly

        self.assertIsInstance(self.CR3BP_dynamics, Dynamics)
        self.assertEqual(self.CR3BP_dynamics.model, self.model_ref)
        self.assertEqual(self.CR3BP_dynamics.library, self.library_ref)
        self.assertEqual(self.CR3BP_dynamics.pert_flag, None)
        npt.assert_allclose(self.CR3BP_dynamics.rvs, self.rvs_ref)

    def test_propagation(self):
        # Test propagation interface, compare with mubody IC propagation

        state_ref = self.mubody_mission.sat.orbit.tra_df.tail(1).values.flatten()
        state = self.CR3BP_dynamics.propagator(self.sat, self.tf)
        state_adim, _ = self.CR3BP_dynamics._propagator_core(
            self.x0_adim, self.tf_adim, 0, 0, 2
        )
        state_adim_ref = state_ref / self.rvs_ref

        npt.assert_allclose(state, state_ref)
        npt.assert_allclose(state_adim, state_adim_ref)


if __name__ == "__main__":
    unittest.main()
