import unittest
from unittest.mock import patch
import numpy as np
import numpy.testing as npt
from ffnavpy.control_laws import (
    ControlAlgorithm,
    DummyControl,
    LinearRegulator,
    LQR,
    MPC,
    ETM,
)
from ffnavpy.satellite import OBC
from ffnavpy.dynamics import Dynamics
from ffnavpy.utils import compute_Q, compute_R


class ControlAlgorithmTest(unittest.TestCase):
    def test_run(self):
        # Test inheritance from abstract class for controller
        class ControllerTest(ControlAlgorithm):
            def __init__(self, a):
                self.a = a

            def run(self):
                return self.a

        algorithm = ControllerTest(5)
        u_cmd = algorithm.run()

        self.assertIsInstance(algorithm, ControlAlgorithm)
        self.assertEqual(u_cmd, 5)

    def test_abstract_class(self):
        # Test the abstract class
        @patch.multiple(ControlAlgorithm, __abstractmethods__=set())
        def test(self):
            self.instance = ControlAlgorithm()

        test(self)
        u_cmd = self.instance.run()

        self.assertIsInstance(self.instance, ControlAlgorithm)
        npt.assert_equal(u_cmd, 0)


class DummyControlTest(unittest.TestCase):
    def test_run(self):
        # Test dummy control
        algorithm = DummyControl()
        u_cmd = algorithm.run(5, value=1)
        u_cmd_abstract = DummyControl.run(5, value=1)

        npt.assert_almost_equal(u_cmd, 0)
        npt.assert_almost_equal(u_cmd_abstract, 0)


class LinearRegulatorTest(unittest.TestCase):
    def setUp(self):
        # System matrix A
        self.A = np.array([[1.1269, -0.4940, 0.1129], [1.0000, 0, 0], [0, 1.0000, 0]])

        self.B = np.array([-0.3832, 0.5919, 0.5191]).reshape(-1, 1)

        self.C = np.array([[1, 0, 0]])

        self.poles = np.array([-0.1 - 0.1j, -0.1 + 0.1j, -0.1])

        self.control = LinearRegulator(self.A, self.B, self.C, self.poles)
        self.x0 = np.array([1, 0, -3])
        self.xref = np.zeros(3)

    def test_run(self):
        # Test algorithm convergence
        x = self.x0.copy()

        for _ in range(500):
            u_cmd = self.control.run(x, self.xref)
            x = (self.A + np.eye(3)) @ x + self.B @ u_cmd

        self.assertIsInstance(u_cmd, np.ndarray)
        self.assertEqual(u_cmd.shape, (1,))
        npt.assert_almost_equal(x, 0)

    def test_compute_k(self):
        # Test k computation

        k = self.control.compute_k()

        self.assertIsInstance(k, np.ndarray)
        self.assertEqual(k.shape, (1, 1))

    def test_compute_K(self):
        # Test K computation

        K = self.control.compute_K()

        self.assertIsInstance(K, np.ndarray)
        self.assertEqual(K.shape, (1, 3))


class LQRTest(unittest.TestCase):
    def setUp(self):
        # System matrix A
        self.A = np.array([[1.1269, -0.4940, 0.1129], [1.0000, 0, 0], [0, 1.0000, 0]])

        self.B = np.array([-0.3832, 0.5919, 0.5191]).reshape(-1, 1)

        self.C = np.array([[1, 0, 0]])

        self.Q = 1 / (1e-5**2) * np.eye(3)
        self.R = 1 / (1e-5**2) * np.eye(1)

        self.control = LQR(self.A, self.B, self.C, self.Q, self.R)
        self.x0 = np.array([1, 0, -3])
        self.xref = np.zeros(3)

    def test_run(self):
        # Test algorithm convergence
        x = self.x0.copy()

        for _ in range(500):
            u_cmd = self.control.run(x, self.xref)
            x = (self.A + np.eye(3)) @ x + self.B @ u_cmd

        self.assertIsInstance(u_cmd, np.ndarray)
        self.assertEqual(u_cmd.shape, (1,))
        npt.assert_almost_equal(x, 0)

    def test_compute_k(self):
        # Test k computation

        k = self.control.compute_k()

        self.assertIsInstance(k, np.ndarray)
        self.assertEqual(k.shape, (1, 1))

    def test_compute_K(self):
        # Test K computation

        K = self.control.compute_K()

        self.assertIsInstance(K, np.ndarray)
        self.assertEqual(K.shape, (1, 3))


class ETMTest(unittest.TestCase):
    def setUp(self):
        # Dynamics to retrieve adimensionalization parameters
        dyn = Dynamics()
        self.rvs = dyn.rvs
        ts = dyn.T

        # Time step for the discrete system
        dT = 600  # [s]

        # Computer setup to retrieve matrices
        Tsk = dT
        obc = OBC({"Tsk": Tsk})
        A = obc.configOBC.A
        self.B = obc.configOBC.B
        self.STM = obc.configOBC.STM

        # Period betweem maneuvers
        period = 600  # [s]
        period_adim = period / ts

        # Controller
        self.control = ETM(A, period_adim)

        # Initial relative state
        self.x0 = np.array(
            [
                150,
                120,
                80,
                0.001,
                -0.002,
                0.0005,
            ]
        )  # [m / m/s]

        self.x0_adim = self.x0 / self.rvs

        # Target relative state
        self.x_target = np.array([100, 100, 100, 0, 0, 0]) / self.rvs

    def test_run(self):
        # Test algorithm convergence
        nlong = 5
        x_actual = self.x0_adim.copy()

        for _ in range(nlong):
            u_cmd = self.control.run(x_actual, self.x_target)
            x_actual = self.STM @ x_actual + self.B @ u_cmd

        self.assertIsInstance(u_cmd, np.ndarray)
        self.assertEqual(u_cmd.shape, (3,))
        npt.assert_allclose(x_actual[:3], self.x_target[:3], atol=1e-5)
        npt.assert_allclose(x_actual[3:6], self.x_target[3:6], atol=1e-5)


class MPCTest(unittest.TestCase):
    def setUp(self):
        # Dynamics to retrieve adimensionalization parameters
        dyn = Dynamics()
        self.rvs = dyn.rvs
        rs = dyn.L
        ts = dyn.T
        vs = rs / ts

        # Time step for the discrete system
        dT = 100  # [s]

        # Computer setup to retrieve matrices
        Tsk = dT
        obc = OBC({"Tsk": Tsk})
        A = obc.configOBC.STM
        B = obc.configOBC.B
        C = obc.configOBC.C

        # Weight matrices
        x_threshold = np.concatenate((np.ones(3) * 1, np.ones(3) * 0.001)) / self.rvs
        Q = compute_Q(x_threshold)
        u_threshold = 0.01 * np.ones(3) / vs
        R = compute_R(u_threshold)

        # Horizon and maximum control
        self.N = 20
        umax = np.ones(3) * 0.01 / vs

        # Setup control
        self.control = MPC(A, B, C, Q, R, self.N, umax, Tsk)

    def test_run(self):
        # Test algorithm convergence
        nlong = 10 * self.N
        dx_ref = np.zeros(6)
        dx = np.array([30, -54, -12, 0.01, -0.02, -0.005]) / self.rvs
        dx_actual = dx

        for _ in range(nlong):
            u_cmd = self.control.run(dx_actual, dx_ref)
            dx_actual = self.control.A @ dx_actual + self.control.B @ u_cmd

        self.assertIsInstance(u_cmd, np.ndarray)
        self.assertEqual(u_cmd.shape, (3,))
        npt.assert_almost_equal(dx_actual, 0)


if __name__ == "__main__":
    unittest.main()
