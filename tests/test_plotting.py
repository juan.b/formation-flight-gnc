import unittest
from unittest.mock import patch
import numpy as np
import matplotlib.pyplot as plt

from ffnavpy.plotting import (
    plot_trajectory,
    plot_poles,
    plot_blocks,
    plot_position,
    plot_state,
)


class TestPlotTrajectory(unittest.TestCase):
    def test_plot_trajectory(self):
        # Create test data
        trajectory = np.array([[0, 0, 0, 0], [1, 1, 1, 1], [2, 2, 2, 2]])

        # Execute the function under test
        with patch(
            "matplotlib.pyplot.show"
        ):  # Prevent the figure from being shown during tests
            result = plot_trajectory(trajectory)

        # Verify that the function returns the expected value
        self.assertEqual(result, 0)


class TestPlotPoles(unittest.TestCase):
    def test_plot_poles(self):
        # Create test data
        poles = np.array([1 + 2j, -3 + 4j, 5 - 6j])

        # Execute the function under test
        with patch("matplotlib.pyplot.show"):
            fig, ax = plot_poles(poles)

        # Verify that the function returns the figure and axes
        self.assertIsInstance(fig, plt.Figure)
        self.assertIsInstance(ax, plt.Axes)


class TestPlotBlocks(unittest.TestCase):
    def test_plot_blocks(self):
        # Create test data
        x = np.array([0, 1, 2, 3])
        y = np.array([[1, 2, 3, 4], [5, 6, 7, 8]])
        yhat = np.array([[2, 3, 4, 5], [6, 7, 8, 9]])
        P = np.array([[0.1, 0.2], [0.3, 0.4]])
        title = "Test Title"
        xlabel = "Test XLabel"
        ylabels = ["Test YLabel 1", "Test YLabel 2"]

        # Execute the function under test
        with patch("matplotlib.pyplot.show"):
            fig, ax = plot_blocks(x, y, yhat, P, title, xlabel, ylabels)
            fig_2, ax_2 = plot_blocks(x, y, None, None, title, xlabel, ylabels)
            fig_3, ax_3 = plot_blocks(x, y, yhat, None, title, xlabel, ylabels)

        # Verify that the function returns the figure and axes
        self.assertIsInstance(fig, plt.Figure)
        self.assertIsInstance(ax, plt.Axes)
        self.assertIsInstance(fig_2, plt.Figure)
        self.assertIsInstance(ax_2, plt.Axes)
        self.assertIsInstance(fig_3, plt.Figure)
        self.assertIsInstance(ax_3, plt.Axes)


class TestPlotPosition(unittest.TestCase):
    def test_plot_position(self):
        # Create test data
        x = np.array([0, 1, 2, 3])
        y = np.array([[1, 2, 3, 4], [5, 6, 7, 8]])
        yhat = np.array([[2, 3, 4, 5], [6, 7, 8, 9]])
        P = np.array([[0.1, 0.2], [0.3, 0.4]])
        units = ("s", "m")

        # Execute the function under test
        with patch("matplotlib.pyplot.show"):
            fig, ax = plot_position(x, y, yhat, P, units)

        # Verify that the function returns the figure and axes
        self.assertIsInstance(fig, plt.Figure)
        self.assertIsInstance(ax, plt.Axes)


class TestPlotState(unittest.TestCase):
    def test_plot_state(self):
        # Create test data
        x = np.array([0, 1, 2, 3])
        y = np.array([[1, 2, 3, 4], [5, 6, 7, 8]])
        yhat = np.array([[2, 3, 4, 5], [6, 7, 8, 9]])
        P = np.array([[0.1, 0.2], [0.3, 0.4]])
        units = ("s", "m", "m/s")

        # Execute the function under test
        with patch("matplotlib.pyplot.show"):
            fig, ax = plot_state(x, y, yhat, P, units)

        # Verify that the function returns the figure and axes
        self.assertIsInstance(fig, plt.Figure)
        self.assertIsInstance(ax, plt.Axes)


if __name__ == "__main__":
    unittest.main()
