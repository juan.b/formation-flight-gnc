import unittest

import numpy as np
import numpy.testing as npt
from astropy.coordinates import cartesian_to_spherical as c2s
from astropy.coordinates import spherical_to_cartesian as s2c

from ffnavpy.utils import (
    build_A_ineq,
    build_A_stack,
    build_b_ineq,
    build_B_stack,
    build_Q_stack,
    build_R_stack,
    cartesian_to_spherical,
    compute_A,
    compute_mu,
    compute_STM,
    compute_xL2,
    define_poles,
    norm,
    spherical_to_cartesian,
    compute_characteristic_time,
    compute_linear_system_bandwidth,
    compute_bandwidth_from_tau,
)


class UtilsTest(unittest.TestCase):
    def setUp(self):
        # Setup test parameters
        self.m1 = 1.98847e30  # Sun mass
        self.m2 = 5.9722e24 + 7.342e22  # Earth/Moon mass

        # Compute mu manually
        self.mu = self.m2 / (self.m1 + self.m2)

        # L2 position (from barycenter) for Sun-Earth/Moon system
        self.xL2 = 1.0100750946886012

    def test_compute_A(self):
        # Test the computation of A for Sun-Earth system
        mu = 3.003480593992993e-06
        rL2 = 1.0100038658320006

        A_ref = np.array(
            [
                [0.0, 0.0, 0.0, 1.0, 0.0, 0.0],
                [0.0, 0.0, 0.0, 0.0, 1.0, 0.0],
                [0.0, 0.0, 0.0, 0.0, 0.0, 1.0],
                [8.93573393, 0.0, 0.0, 0.0, 2.0, 0.0],
                [0.0, -2.96786696, 0.0, -2.0, 0.0, 0.0],
                [0.0, 0.0, -3.96786696, 0.0, 0.0, 0.0],
            ]
        )

        A_test = compute_A(mu, rL2)

        npt.assert_almost_equal(A_test, A_ref, decimal=6)

    def test_compute_STM(self):
        # Test the computation of A for Sun-Earth system
        mu = 3.003480593992993e-06
        rL2 = 1.0100038658320006

        STM_ref = np.array(
            [
                [0.0, 0.0, 0.0, 1.0, 0.0, 0.0],
                [0.0, 0.0, 0.0, 0.0, 1.0, 0.0],
                [0.0, 0.0, 0.0, 0.0, 0.0, 1.0],
                [8.93573393, 0.0, 0.0, 0.0, 2.0, 0.0],
                [0.0, -2.96786696, 0.0, -2.0, 0.0, 0.0],
                [0.0, 0.0, -3.96786696, 0.0, 0.0, 0.0],
            ]
        ) + np.eye(6)

        STM_test = compute_STM(mu, rL2, 1)

        npt.assert_almost_equal(STM_test, STM_ref, decimal=6)

    def test_compute_xL2(self):
        # Test L2 position calculation
        xL2_test = compute_xL2(self.mu)

        npt.assert_allclose(xL2_test, self.xL2)

    def test_compute_mu(self):
        # Test primaries mass ratio calculation
        mu_test = compute_mu(self.m1, self.m2)

        npt.assert_almost_equal(mu_test, self.mu)

    def test_define_poles(self):
        # Test the computation of poles
        w0 = 1.3e4
        p_ref = np.array(
            [
                -3364.64758633 + 12557.03574176j,
                -3364.64758633 - 12557.03574176j,
                -12557.03574176 + 3364.64758633j,
                -12557.03574176 - 3364.64758633j,
                -9192.38815543 - 9192.38815543j,
                -9192.38815543 + 9192.38815543j,
            ]
        )

        p_test = define_poles(w0)

        npt.assert_almost_equal(p_test, p_ref, decimal=6)

    def test_cartesian_to_spherical(self):
        # Test transformation from cartesian to spherical coordinates
        u = np.random.random((3, 10)) * 10
        sph_coord_ref = c2s(*u)
        sph_coord = cartesian_to_spherical(u)

        npt.assert_almost_equal(sph_coord, sph_coord_ref)

    def test_spherical_to_cartesian(self):
        # Test transformation from spherical to cartesian coordinates
        u = np.random.random((3, 10))

        sph_coord_ref = s2c(*u)
        sph_coord = spherical_to_cartesian(u)

        npt.assert_almost_equal(sph_coord, sph_coord_ref)

    def test_norm(self):
        # Test norm function and its spectral variant
        v = np.array([1, 2, 3])
        A = np.array([[1, 2], [3, 4]])

        v_norm = np.sqrt(v[0] ** 2 + v[1] ** 2 + v[2] ** 2)
        A_norm = np.sqrt(np.max(np.abs(np.linalg.eig(A.T @ A)[0])))

        npt.assert_allclose(norm(v), v_norm)
        npt.assert_allclose(norm(A, "spectral"), A_norm)

    def test_linear_system_bandwidth(self):
        # Test calculation of the bandwith of a linear system from its matrix
        A = np.array([[1, 2], [3, 4]])
        W_expected = np.sqrt(norm(A, "spectral") / norm(np.linalg.inv(A), "spectral"))

        W = compute_linear_system_bandwidth(A)

        npt.assert_almost_equal(W, W_expected)

    def test_characteristic_time(self):
        # Test computation of characteristic time of a linear system from its
        # matrix
        A = np.array([[1, 2], [3, 4]])

        tau_expected = 1 / np.sqrt(
            norm(A, "spectral") / norm(np.linalg.inv(A), "spectral")
        )

        tau = compute_characteristic_time(A, 1.0)

        npt.assert_almost_equal(tau, tau_expected)

    def test_bandwidth_from_tau(self):
        # Test computation of bandwidth of a linear system from its
        # characteristic time
        A = np.array([[1, 2], [3, 4]])

        tau = 1 / np.sqrt(norm(A, "spectral") / norm(np.linalg.inv(A), "spectral"))
        W_expected = 1 / tau

        W = compute_bandwidth_from_tau(tau, 1.0)

        npt.assert_almost_equal(W, W_expected)


class MatrixBuilderTest(unittest.TestCase):
    def setUp(self):
        # Create the stacked matrices for the tests
        self.n, self.m = 3, 2
        self.A = np.random.random((self.n, self.n))
        self.B = np.random.random((self.n, self.m))
        self.C = np.random.random((self.n, self.n))
        self.R = np.random.random((self.m, self.m))
        self.Q = np.random.random((self.n, self.n))
        self.DeltaX = np.random.random(self.n)

        self.Rkoz = np.random.random()
        self.N = 3

    def test_build_A_stack(self):
        # Test shape, diagonal and out of diagonal
        A_stack = build_A_stack(self.A, self.N)
        expected_shape = (self.N * self.n, self.n)

        An = np.eye(self.n)
        for _ in range(self.N):
            An = An @ self.A

        self.assertEqual(A_stack.shape, expected_shape)
        npt.assert_almost_equal(A_stack[: self.n, :], self.A)
        npt.assert_almost_equal(A_stack[-self.n :, :], An)

    def test_build_B_stack(self):
        # Test shape, diagonal and out of diagonal
        B_stack = build_B_stack(self.A, self.B, self.N)
        expected_shape = (self.N * self.n, self.N * self.m)

        An = np.eye(self.n)
        for _ in range(self.N - 1):
            An = An @ self.A

        Bn = An @ self.B

        self.assertEqual(B_stack.shape, expected_shape)
        npt.assert_almost_equal(B_stack[: self.n, : self.m], self.B)
        npt.assert_almost_equal(B_stack[: self.n, -self.m :], np.zeros_like(self.B))
        npt.assert_almost_equal(B_stack[-self.n :, : self.m], Bn)

    def test_build_Q_stack(self):
        # Test shape, diagonal and out of diagonal
        Q_stack = build_Q_stack(self.Q, self.N)
        expected_shape = (self.N * self.n, self.N * self.n)

        self.assertEqual(Q_stack.shape, expected_shape)
        npt.assert_almost_equal(Q_stack[: self.n, : self.n], self.Q)
        npt.assert_almost_equal(
            Q_stack[-self.n :, : self.n], np.zeros((self.n, self.n))
        )

    def test_build_R_stack(self):
        # Test shape, diagonal and out of diagonal
        R_stack = build_R_stack(self.R, self.N)
        expected_shape = (self.N * self.m, self.N * self.m)

        self.assertEqual(R_stack.shape, expected_shape)
        npt.assert_almost_equal(R_stack[: self.m, : self.m], self.R)
        npt.assert_almost_equal(
            R_stack[-self.m :, : self.m], np.zeros((self.m, self.m))
        )

    def test_build_A_ineq(self):
        # Test shape
        B_stack = build_B_stack(self.A, self.B, self.N)
        A_ineq = build_A_ineq(self.DeltaX, B_stack, self.C, self.N)

        expected_shape = (self.N, self.N * self.m)

        self.assertEqual(A_ineq.shape, expected_shape)

    def test_build_b_ineq(self):
        # Test shape
        A_stack = build_A_stack(self.A, self.N)
        b_ineq = build_b_ineq(self.DeltaX, A_stack, self.C, self.Rkoz, self.N)

        expected_shape = (self.N,)

        self.assertEqual(b_ineq.shape, expected_shape)


if __name__ == "__main__":
    unittest.main()
