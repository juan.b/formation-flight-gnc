import numpy as np

from ffnavpy.analysis import run_analysis
from ffnavpy.plotting import save_plots, plot_results, plot_control
from ffnavpy.satellite import CommandSequence
import matplotlib.pyplot as plt
from tqdm.auto import tqdm
# Initial state of deputy [km, km/s]

x0 = np.array(
    [
        1.5097937630e11,
        -4.6840029080e-08,
        1.8369701987e-08,
        -6.0196990433e-15,
        1.5667113355e02,
        -1.1858897424e02,
    ]
)  # [m / m/s], 'P1-IdealSynodic', Sun-Earth


# Initial relative state [m, m/s]
dx0 = np.array([100, 100, 100, np.random.random()/10000, np.random.random()/10000, np.random.random()/10000])



# State commanded
x_target = np.array([100, 100, 100, 0, 0, 0])

# Duration of the simulation [s]
tf = 3600*3

# Time step of simulation [s]
dT = 1

configOBC = {'x_target': x_target,
             'estimation_mode': 'kalman',
             'control_mode': 'MPC',
             'Tsk': 1,
             'sigma_r0': 1e-7,
             'sigma_v0': 1e-8,
             'sigma_x': 1e-4,
             'sigma_y': 1e-4,
             'sigma_z': 1e-4,
             'period': 60,
             'horizon': 10,
             'u_threshold_v': np.ones(3) * 0.00015,
             'x_threshold': np.concatenate([0.01*np.ones(3),0.0001*np.ones(3)]),
             'thrust_nominal' : 1e-3,
             'tb_min' : 0.01,
             'tb_lower_limit': 0.008,
             'X0' : dx0
             }

configSim = {'sigma_distance': 1e-06, 'sigma_angle': 0.25}

cmd_sequence = CommandSequence()
# cmd_sequence.add_command('change_control_mode', 'LQR', 60)
# cmd_sequence.add_command('change_estimation_mode', 'kalman', 3000)
# cmd_sequence.add_command('change_control_mode', 'LQR', 4800)
# cmd_sequence.add_command('change_control_mode', 'ETM', 3600*2)

# Create predefined case for chief-deputy formation
# simcase = ChiefDeputyCase(LiteBird, CalSat, cmd_sequence=cmd_sequence, configOBC=ConfigOBC)
sim_lqr = run_analysis(x0, dx0, x_target, tf, dT, configOBC, configSim, cmd_seq=cmd_sequence)

fig_dict_lqr = plot_results(sim_lqr)
plt.show()
