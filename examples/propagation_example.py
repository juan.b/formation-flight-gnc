from mubody.mission import Mission

from astropy.time import Time
import numpy as np
import matplotlib.pyplot as plt

from ffnavpy.simulator import Simulator
from ffnavpy.satellite import Satellite
from ffnavpy.simcase import ChiefDeputyCase


# generate IC for FETBP
time_sempy = Time("2000-01-01 12:00:00.000", scale="tdb").utc

# create Mission for halo orbits in FETBP model and with time of sempy
upm = Mission(
    orbit="Lissajous", model="FETBP", date=time_sempy, mission_time=180 * 86400
)

# generate halo orbit
upm.OTM()

# change coordinates to Sun-J2000Eq
# upm.coordinate_frame_change("Sun-J2000Eq")

# Retrieve Earth position in initial time
s = np.concatenate(
    (upm.DS.eph_class.r("Earth", 0), upm.DS.eph_class.v("Earth", 0)), axis=0
)

x0 = (upm.sat.orbit.IC()[0] - s).squeeze() / 1000

# x0 = np.array(
#     [
#         1.50464846e08,
#         0.00000000e00,
#         1.85588146e06,
#         -1.99045682e-15,
#         -6.04730575e-02,
#         -4.44642929e-14,
#     ]
# )

x0_cs = x0 + np.array([0.1, 0.1, 0.1, 0, 0, 0])

tf = 3600

upmsat = Satellite(x0)
calsat = Satellite(x0_cs)
simcase = ChiefDeputyCase(upmsat, calsat)

sim = Simulator(simcase, 2, "CR3BP", library="mubody")

sim.simulate(tf)

r1 = sim.sats[0].trajectory.states
r2 = sim.sats[1].trajectory.states

plt.plot((r2 - r1)[:, 0:3])
plt.show()
